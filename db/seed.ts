import { Pic, db } from 'astro:db';
import { faker } from '@faker-js/faker';

// https://astro.build/db/seed
export default async function seed() {
	await db.insert(Pic).values([
		{ name: `${faker.word.adverb()} ${faker.word.preposition()} ${faker.word.noun()}.`, url: faker.image.url(), },
		{ name: `${faker.word.adverb()} ${faker.word.preposition()} ${faker.word.noun()}.`, url: faker.image.url(), },
		{ name: `${faker.word.adverb()} ${faker.word.preposition()} ${faker.word.noun()}.`, url: faker.image.url(), },
		{ name: `${faker.word.adverb()} ${faker.word.preposition()} ${faker.word.noun()}.`, url: faker.image.url(), },
	])
}
