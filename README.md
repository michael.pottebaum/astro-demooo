# Demooo

This is a demo of [Astro DB](https://astro.build/db/) using a `<form>` in SSR mode. For more detailed info on Astro DB as a service, check out [this blog post](https://astro.build/blog/astro-db-deep-dive/) and [this Astro Studio recipe](https://docs.astro.build/en/recipes/studio/).

![d'mooo](/public/astro-cow.jpg)
\
\
[source](https://travisseekart.com/the-cow-that-jumped-over-the-moo-n)

## Requirements

- [NodeJS](https://nodejs.org/en) - I used [pnpm](https://pnpm.io/installation) a package manger, but feel free to substitute Node's built-in `npm` for any commands.

## Run

From a terminal in root directory run:
```
pnpm install
pnpm run dev
```

## Steps to reproduce

1. [Create Astro project with pnpm](https://docs.astro.build/en/install/auto/)
2. [Follow recipe for using `<form>` tags in Astro pages](https://docs.astro.build/en/recipes/build-forms/) and [use the node adapter](https://docs.astro.build/en/guides/integrations-guide/node/)
3. [Add Tailwind for style](https://docs.astro.build/en/guides/integrations-guide/tailwind/)
4. Build `pages/01-form.astro` - form w/ tailwind css
5. [Follow recipe for Astro DB](https://docs.astro.build/en/guides/astro-db/)
6. Build `pages/02-get-all.astro` - get all pics from db and render them
7. Build `pages/03-create.astro` - wire up form to db

## 🚀 Project Structure

Inside of your Astro project, you'll see the following folders and files:

```text
/
├── public/
│   └── favicon.svg
├── src/
│   ├── components/
│   │   └── Card.astro
│   ├── layouts/
│   │   └── Layout.astro
│   └── pages/
│       └── index.astro
└── package.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `public/` directory.

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                   | Action                                           |
| :------------------------ | :----------------------------------------------- |
| `pnpm install`             | Installs dependencies                            |
| `pnpm run dev`             | Starts local dev server at `localhost:4321`      |
| `pnpm run build`           | Build your production site to `./dist/`          |
| `pnpm run preview`         | Preview your build locally, before deploying     |
| `pnpm run astro ...`       | Run CLI commands like `astro add`, `astro check` |
| `pnpm run astro -- --help` | Get help using the Astro CLI                     |

## 👀 Want to learn more?

Feel free to check [our documentation](https://docs.astro.build) or jump into our [Discord server](https://astro.build/chat).
